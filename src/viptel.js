const crypto = require("crypto");
const { EventEmitter } = require("events");
const WS = require("ws");
const ReconnectingWebSocket = require("reconnecting-websocket");
const moment = require("moment");
const request = require("request-promise-native");

class Viptel extends EventEmitter {
  constructor(username, password) {
    super();
    this.username = username;
    this.password = password;
    this.nonce = "";
    this.lastData = null;
  }

  _sha1(s) {
    return crypto.createHash("sha1").update(s).digest("hex");
  }

  _hashPassword() {
    return this._sha1(
      this._sha1(`${this.username}:${this.password}`) + ":" + this.nonce
    );
  }

  connect(cb = false) {
    const viptel = this;
    const opts = {};

    const WebSocket = class extends WS {
      constructor(url) {
        super(url, opts);
      }
    };

    const socket = new ReconnectingWebSocket(
      "wss://pbxmanager.viptel.sk:8088",
      [],
      { WebSocket }
    );

    socket.addEventListener("open", function () {
      if (viptel.nonce) {
        socket.send(
          JSON.stringify({
            action: "user.login",
            username: viptel.username,
            password: viptel._hashPassword(),
          })
        );
      }
    });

    socket.addEventListener("message", function (data) {
      try {
        console.debug("viptel message", { type: data.type, data: data.data });
        if (data.type === "message") {
          const d = JSON.parse(data.data);
          switch (d.code) {
            case 100:
              // nonce received and possible to login
              viptel.nonce = d.nonce;
              socket.send(
                JSON.stringify({
                  action: "user.login",
                  username: viptel.username,
                  password: viptel._hashPassword(),
                })
              );
              break;
            case 202:
              if (cb) {
                // run callback on first successful login
                cb();
                cb = false;
              }
              break;
          }

          if (d.event) {
            viptel.emit(d.event, d);
          }
        }
      } catch (ex) {
        console.error(ex, "wrong message from VipTel");
      }
    });

    socket.addEventListener("error", function (e) {
      if (e.message !== "timeout") {
        console.error(e.error, "socket");
      }
    });

    this.socket = socket;
  }

  callPhone(from, to) {
    const d = {
      action: "call.create",
      from,
      to,
      call_random_id: Date.now(),
    };
    this.socket.send(JSON.stringify(d));
  }

  async cdr(fromDate = false) {
    const r = await request("https://pbxmanager.viptel.sk/api/cdr", {
      qs: {
        date_from: (fromDate ? moment(fromDate) : moment()).format(
          "YYYY-MM-DD"
        ),
      },
      auth: {
        user: this.username,
        pass: this.password,
      },
      json: true,
    });
    return r;
  }
}

module.exports = Viptel;
