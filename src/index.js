const yargs = require("yargs");
const Viptel = require("./viptel");

const argv = require("yargs")
  .scriptName("viptel-tool")
  .usage("$0 [args]")
  .options("user", {
    alias: "u",
    demandOption: true,
    description: "Viptel username",
  })
  .options("password", {
    alias: "P",
    demandOption: true,
    description: "Viptel password",
  })
  .options("ext", {
    alias: "e",
    description: "Phone extension provided by Viptel",
    default: "10",
  })
  .options("call", {
    alias: "c",
    description: "Phone number in international format to be called.",
  })
  .options("test", {
    alias: "t",
    description: "Perform some tests.",
  })
  .options("monitor", {
    alias: "m",
    type: "boolean",
    description:
      "Monitor Viptel PBX activity and print information to console.",
  })
  .help().argv;

const viptel = new Viptel(argv.user, argv.password);

if (argv.monitor) {
  viptel.on("call.begin", function (data) {
    console.log("begin", data);
  });

  viptel.on("call.end", function (data) {
    console.log("end", data);
  });

  viptel.on("call.pickup", function (data) {
    console.log("pickup", data);
  });
}

viptel.connect(async () => {
  try {
    if (argv.call) {
      await viptel.callPhone(argv.ext, argv.call);
      if (!argv.monitor) {
        console.log("waiting for call...");
        await new Promise((resolve) => {
          viptel.on("call.end", function (data) {
            console.log("call ended");
            resolve();
          });
        });
      }
    }

    if (argv.test) {
      const d = await viptel.cdr();
      console.log("count of calls today:", d.length);
      console.log(
        "count of recordings:",
        d.filter((a) => a.recording_file).length
      );
    }

    if (argv.monitor) {
      console.log("Monitoring is active. Press Ctrl+C to exist");
    } else {
      process.exit(0);
    }
  } catch (ex) {
    console.error(ex);
    process.exit(1);
  }
});
