# VipTel Tool

## Install

```
git clone  https://gitlab.com/curo-open/viptel-tool.git
cd viptel-tool
yarn
```

## Run

```
# perform tests (download CDR)
yarn start -u user -P password -t

# monitor (stay connected to PBX and print events to console)
yarn start -u user -P password -m

# call number from extension 10
yarn start -u user -P password -ext 10 -c "004219xxxxxxxx"
```
